<?php

header("access-control-allow-origin: *");
header("access-control-allow-headers: Content-Type, Authorization");

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

/**
 * CardinalStone's RESTFUL API starts here
 * 
 */

require_once('../app/api/config/constant.php');

require_once('../app/api/customer/customer.php');

require_once('../app/api/contactmanager/contactmanager.php');

require_once('../app/api/stockbroking/stockbroking.php');

require_once('../app/api/cashaccount/cashaccount.php');

require_once('../app/api/fixedincome/fixedincome.php');

require_once('../app/api/security/security.php');

require_once('../app/api/paymentgateway/paymentgateway.php');

require_once('../app/api/order/order.php');

require_once('../app/api/trade/trade.php');

require_once('../app/api/watchlist/watchlist.php');

require_once('../app/api/dashboard/dashboard.php');

// Run app
$app->run();
