<?php

use models\OrderModel;
use middleware\Authentication as auth;

/**
 * 
 */
$app->get('/api/findCustomerOrders/'
        . '{customerId}/'
        . '{cacheTrigger}'
        , function( $request, $response ) {

    $orderModel = new OrderModel(); //Instantiates the Trade Model
    //Sets the parameters for the Trade Order Object
    $orderModel->customerId = $request->getAttribute('customerId');
    $orderModel->cacheTrigger = $request->getAttribute('cacheTrigger');

    $result = $orderModel->findCustomerOrders();

    //Returns the response object to the user
    return $response->withStatus(200)
                    ->withJson($result, 200);
})->add(new auth());


$app->post('/api/searchCustomerOrders', function( $request, $response ) {  
    $postData = $request->getParsedBody();
    $orderModel = new OrderModel(); //Instantiates the Order Model  
    //Sets the parameters for the Trade Order Object
    $orderModel->customerId = $postData['customerId'];
    $orderModel->startDate = $postData['startDate'];
    $orderModel->endDate = $postData['endDate'];
    
    $result = $orderModel->searchCustomerOrders();
    //Returns the response object to the user
    return $response->withStatus(200)
                    ->withJson($result, 200);
})->add(new auth());
