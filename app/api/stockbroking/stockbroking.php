<?php

use models\StockbrokingModel;

/**
 * This method returns both Exchange Portfolios
 *
 * @author Olaleye Osunsanya
 *
 * @param int $id The client's id
 * */
$app->get('/api/findCustomerPortfoliosById/{id}', function( $request, $response ) {
    //gets the id from the request object
    $id = $request->getAttribute('id');
    
    $stockbrokingModel = new StockbrokingModel();// Instantiates the Stockbroking model

    //Returns the response object to the user
    return $response->withStatus(200)
                    ->withJson($stockbrokingModel->findPortfoliosByCustomerId($id), 200);
});