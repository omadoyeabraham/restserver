<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use models\WatchListModel;
use middleware\Authentication;

/**
 * 
 */
$app->get('/api/getWatchList/{id}', function($request, $response){
    $watchlistModel = new WatchListModel();
    $watchlistModel->customerId = (int) $request->getAttribute('id');       
    $result = $watchlistModel->getWatchList();
    return $response->withStatus(200)
                        ->withJson($result, 200);
})->add(new Authentication());

$app->post('/api/createWatchList', function($request, $response) {
    $postData = $request->getParsedBody();
    $watchlistModel = new WatchListModel();

    $watchlistModel->customerId = (int) $postData['customerId'];
    $watchlistModel->securityName = "'" . $postData['securityName'] . "'";
    $watchlistModel->price = "'" . $postData['price'] . "'";
    $watchlistModel->conditionName = "'" . WatchListModel::$conditions[$postData['condition']] . "'";
    $watchlistModel->condition = "'" . $postData['condition'] . "'";
    $watchlistModel->name = "'" . $postData['watchlistname'] . "'";

    $result = $watchlistModel->createWatchList();
    //Returns the response object to the user
    if ($result != FALSE) {
        return $response->withStatus(200)
                        ->withJson($result, 200);
    } else {
        return $response->withStatus(500);
    }
})->add(new Authentication());

$app->post('/api/updateWatchList', function($request, $response) {
    $postData = $request->getParsedBody();
    $trimmedData = trim($postData['watchlist_name']);
    $watchlistName = (isset($trimmedData) && !empty($trimmedData)) ? $postData['watchlist_name'] : 'null';
    
    $watchlistModel = new WatchListModel();

    $watchlistModel->customerId = (int) $postData['user_id'];
    $watchlistModel->securityName = "'" . $postData['name'] . "'";
    $watchlistModel->price = "'" . $postData['notify_price'] . "'";
    $watchlistModel->conditionName = "'" . WatchListModel::$conditions[$postData['condition']] . "'";
    $watchlistModel->condition = "'" . $postData['condition'] . "'";
    $watchlistModel->name = "'" . $watchlistName . "'";
    $watchlistModel->id = (int) $postData['id'];

    $result = $watchlistModel->editWatchList();

    if ($result != FALSE) {
        return $response->withStatus(200)
                        ->withJson($result, 200);
    } else {
        return $response->withStatus(500);
    }
})->add(new Authentication());

$app->post('/api/deleteWatchList', function($request, $response){
    $postData = $request->getParsedBody();
    $watchlistModel = new WatchListModel();
    
    $watchlistModel->customerId = (int) $postData['customerId'];
    $watchlistModel->id = (int) $postData['watchlistId'];
    
    $result = $watchlistModel->deleteWatchList();
    
    if ($result != FALSE) {
        return $response->withStatus(200)
                        ->withJson($result, 200);
    } else {
        return $response->withStatus(500);
    }
})->add(new Authentication());

$app->post('/api/toggleWatchList', function($request, $response){
    $postData = $request->getParsedBody();
    $watchlistModel = new WatchListModel();
    
    $watchlistModel->customerId = (int) $postData['customerId'];
    $watchlistModel->id = (int) $postData['watchlistId'];
    $currentStatus = (int) $postData['status'];
    $watchlistModel->status = ($currentStatus === 1) ? 0 : 1;
    
    $result = $watchlistModel->toggleWatchList();
    
    if ($result != FALSE) {
        return $response->withStatus(200)
                        ->withJson($result, 200);
    } else {
        return $response->withStatus(500);
    }
})->add(new Authentication());



$app->get('/api/sendWatchListNotification', function($request, $response){
    WatchListModel::sendWatchListNotification();
});
