<?php

use models\DashboardModel;
use middleware\Authentication;

/**
 * This function returns the customer data by their Id
 *
 * @author Olaleye Osunsanya
 *
 * @param int $id The client Id
 * */
$app->get('/api/getDashboardData', function( $request, $response ) {
	//apcu_clear_cache();die;
	
	if (apcu_exists('dashboardData')) {
        //Returns the response object to the user
        return $response->withStatus(200)
                        ->withJson(apcu_fetch('dashboardData'), 200);
    } else {
        $data = DashboardModel::getDashboardData();
        apcu_store('dashboardData', $data, 300);
        return $response->withStatus(200)
                        ->withJson($data, 200);
    }
})->add(new Authentication());


$app->get('/api/dashboardData', function( $request, $response ) {	
    $data = DashboardModel::getDashboardData();
        apcu_store('dashboardData', $data);
});
