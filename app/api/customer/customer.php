<?php

use models\CustomerModel as customerModel;
use middleware\Authentication as auth;

/**
 * This function returns the customer data by their Name and Password
 *
 * @author Olaleye Osunsanya
 *
 * @param int $name The client Name
 * */
$app->post('/api/findCustomerByName', function( $request, $response ) {
    //gets the name from the request object
    $postData = $request->getParsedBody();
	$name = (isset($postData['username'])) ? trim($postData['username']) : "";
    $password = (isset($postData['password'])) ? trim($postData['password']) : "";

    if (!empty($name) && !empty($password)) {
        $customer = new customerModel();
        $data = $customer->login($name, $password);
    }

    //Returns the response object to the user
    if (!empty($data)) {
        return $response->withStatus(200)
                        ->withJson($data, 200);
    } else {
        
        return $response->withStatus(401);
    }
});


/**
* This function returns the customer data by their Username
*
* @author Omadoye Abraham
**/
$app->post('/api/findCustomerByUsername', function($request, $response) {
    $postData = $request->getParsedBody();

    $username = (isset($postData['username'])) ? trim($postData['username']) : "";

    if(!empty($username)) {
        $customer = new customerModel();
        $data = $customer->findCustomerByName($username);
    }

    if(!empty($data)) {
        return $response->withStatus(200)
                        ->withJson($data, 200);
    } else {
        return $response->withStatus(401);
    }
});


$app->post('/api/findCustomerById', function( $request, $response ) {
    //gets the name from the request object
    $postData = $request->getParsedBody();

    if (isset($postData['customerId']) && !empty($postData['customerId'])) {
        $id = $postData['customerId'];
        $customer = new customerModel();
        $data = $customer->findCustomerById($id);
        
        //Returns the response object to the user
        return $response->withStatus(200)
                        ->withJson($data, 200);
    }else{
        return $response->withStatus(500);
    }
});

/**
 * 
 */
$app->post('/api/logout', function($request, $response) {
    $postData = $request->getParsedBody();
    $auth = $request->getHeader('Authorization');
    $token = $auth[0];
    $customer = new customerModel();
    $result = $customer->logout($postData['customerId'], $token);
    //Returns the response object to the user
    return $response->withStatus(200)
                    ->withJson($result, 200);
})->add(new auth());

/**
 * 
 */
$app->post('/api/resetPassword', function($request, $response) {
    $postData = $request->getParsedBody();
    $id = trim($postData['customerId']);
    $oldPassword = trim($postData['oldPassword']);
    $newPassword = trim($postData['newPassword']);
    $customer = new customerModel();

    $result = $customer->resetPassword($id, $oldPassword, $newPassword);

    if (isset($result) && !empty($result)) {
        //Returns the response object to the user
        return $response->withStatus(200)
                        ->withJson($result, 200);
    }else{
        //Returns the response object to the user
    return $response->withStatus(500)
                    ->withJson(['status' => 'Incorrect Password'], 500);
    }
})->add(new auth());

$app->post('/api/changePassword', function($request, $response) {
    $postData = $request->getParsedBody();

    $id = trim($postData['customerId']);
    $newPassword = trim($postData['newPassword']);
    $customer = new customerModel();
    $customerData = $customer->findCustomerDataById($id);
    $oldPassword = $customerData->portalPassword;

    $result = $customer->resetPassword($id, $oldPassword, $newPassword);

    if (isset($result) && !empty($result)) {
        //Returns the response object to the user
        return $response->withStatus(200)
                        ->withJson($result, 200);
    }else{
        //Returns the response object to the user
    return $response->withStatus(500)
                    ->withJson(['status' => 'Incorrect Password'], 500);
    }

});


/**
* Send the user an email with a link to reset their password
*
**/
$app->post('/api/sendPasswordResetLink', function($request, $response) {
    $postData = $request->getParsedBody();
    $username = (isset($postData['username'])) ? trim($postData['username']) : "";
    $userLabel = (isset($postData['userLabel'])) ? trim($postData['userLabel']) : "";
    $email = (isset($postData['email'])) ? trim($postData['email']) : "";
    $userId = (isset($postData['user_id'])) ? trim($postData['user_id']) : "";

    if(($username !== "") && ($email !== "")) {
        $customer = new customerModel();
        $result = $customer->sendResetPasswordLink($username, $email, $userId, $userLabel);

        return $response->withStatus(200)
                        ->withJson($result, 200);

    }else {
        return $response->withStatus(500)
                        ->withJson(['status' => 'Incorrect Username'], 500);
    }
});

/**
* Determine the validity of a password reset link, and ensure that it has not expired
*/
$app->post('/api/validatePasswordResetLink', function($request, $response) {
    return $response->withStatus(200)
                        ->withJson(['valid' => true], 200);
});

$app->post('/api/verifyPasswordResetCode', function($request, $response) {
    $postData = $request->getParsedBody();
    $resetCode = (isset($postData['resetCode'])) ? trim($postData['resetCode']) : "";
    $customer = new customerModel();
    $result = $customer->retrievePasswordResetData($resetCode);
    return $response->withStatus(200)
                    ->withJson($result);
});

