<?php

use models\TermInstrumentModel;
use models\CashAccountModel;

/**
 * This method returns both Naira and Dollar investments
 *
 * @author Olaleye Osunsanya
 *
 * @param int $id The client's id
 * @param int $startRecord Where you want the record to start from
 * @param int $numberOfRecords The total number of records to return
 * @param string $arg3 I have no idea about this parameter, I have to ask Zanibal
 * @param string $status The status of the investment "RUNNING / TERMINATED"
 * @param dateTime $startDate The start date time
 * @param dateTime $endDate The end date time 
 * */
$app->get('/api/findCustomerTermInstruments/'
        . '{id}/'
        . '{status}/'
        . '{startDate}/'
        . '{endDate}', function( $request, $response ) {

    $id = $request->getAttribute('id'); //gets the id from the request object
    $status = $request->getAttribute('status');
    $startDate = $request->getAttribute('startDate');
    $endDate = $request->getAttribute('endDate');

    $cacheId = 'fi' . $status . $id; //create a cache Id

    if (apcu_exists($cacheId)) {
        return $response->withStatus(200)->withJson(apcu_fetch($cacheId), 200);
    } else {
        $termInstrumentModel = new TermInstrumentModel(new CashAccountModel());
        $result = $termInstrumentModel->findCustomerTermInstruments($id, $status, $startDate, $endDate);
        apcu_store($cacheId, $result, 7200);
        return $response->withStatus(200)
                        ->withJson(apcu_fetch($cacheId), 200);
    }

    $termInstrumentModel = new TermInstrumentModel(new CashAccountModel());

    return $response->withStatus(200)
                    ->withJson($termInstrumentModel->findCustomerTermInstruments(
                                    $id, $status, $startDate, $endDate), 200);
});
