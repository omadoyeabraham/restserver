<?php

use models\CashAccountModel;
use middleware\Authentication;

/**
 * This method returns both Naira and Dollar Cash Accounts
 *
 * @author Olaleye Osunsanya
 *
 * @param int $id The client's id
 *  
 * */
$app->get('/api/findCustomerFiAccts/{id}', function( $request, $response ) {

    //gets the id from the request object
    $id = $request->getAttribute('id');

    $cashAccount = new CashAccountModel(); //instantiates CashAccount Model
    //Returns the response object to the user
    return $response->withStatus(200)
                    ->withJson($cashAccount->findCustomerFiAccts($id), 200);
});

/**
 * 
 */
$app->get('/api/findCustomerNariaRateCashAccountId/{id}', function( $request, $response ) {
    //gets the id from the request object
    $id = $request->getAttribute('id');

    $cashAccount = new CashAccountModel(); //instantiates CashAccount Model
    //Returns the response object to the user
    return $response->withStatus(200)
                    ->withJson($cashAccount->findCustomerNariaRateCashAccountId($id), 200);
});

/**
 * 
 */
$app->get('/api/findCustomerLedgerEntries/'
        . '{arg0}/'
        . '{arg1}/'
        . '{arg2}/'
        . '{arg3}/'
        . '{arg4}', function( $request, $response ) {

    //gets the id from the request object
    CashAccountModel::$customerId = $request->getAttribute('arg0');
    CashAccountModel::$startRecord = $request->getAttribute('arg1');
    CashAccountModel::$endRecord = $request->getAttribute('arg2');
    CashAccountModel::$startDate = $request->getAttribute('arg3');
    CashAccountModel::$endDate = $request->getAttribute('arg4');

    //Returns the response object to the user
    return $response->withStatus(200)
                    ->withJson(CashAccountModel::findCustomerLedgerEntries(), 200);
});

$app->post('/api/findFiAcctLedgerEntriesByAccountNumber', function($request, $response){    
    $postData = $request->getParsedBody();
    
    CashAccountModel::$accountNumber = $postData['accountNumber'];
    CashAccountModel::$startRecord = "0";
    CashAccountModel::$endRecord = "100";
    CashAccountModel::$startDate = $postData['startDate'];
    CashAccountModel::$endDate = $postData['endDate'];
    
    //Returns the response object to the user
    return $response->withStatus(200)
                    ->withJson(CashAccountModel::findFiAcctLedgerEntriesByAccountNumber(), 200);
})->add(new Authentication());
