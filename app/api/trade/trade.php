<?php

use models\TradeModel;
use middleware\Authentication;

/**
 * This method returns both Naira and Dollar Cash Accounts
 *
 * @author Olaleye Osunsanya
 *
 * @param int $id The client's id
 *  
 * */
$app->post('/api/createTradeOrder', function( $request, $response ) {
    
    $postData = $request->getParsedBody();
    
    $tradeModel = new TradeModel(); //Instantiates the Trade Model

    //Sets the parameters for the Trade Order Object
    $tradeModel->instrument = $postData['instrument'];
    $tradeModel->limitPrice = $postData['limitPrice'];
    $tradeModel->orderCurrency = $postData['orderCurrency'];
    $tradeModel->orderOrigin = $postData['orderOrigin'];
    $tradeModel->orderTermName = $postData['orderTermName'];
    $tradeModel->orderType = $postData['orderType'];
    $tradeModel->portfolioLabel = $postData['portfolioLabel'];
    $tradeModel->portfolioName = $postData['portfolioName'];
    $tradeModel->priceType = $postData['priceType'];
    $tradeModel->quantityRequested = $postData['quantityRequested'];
    
    $result = $tradeModel->createTradeOrder();

    //Returns the response object to the user
    return $response->withStatus(200)
                    ->withJson($result, 200);
})->add(new Authentication());

$app->post('/api/getTradeOrderTotal', function( $request, $response ) {
    
    $postData = $request->getParsedBody();
    
    $tradeModel = new TradeModel(); //Instantiates the Trade Model

    //Sets the parameters for the Trade Order Object
    $tradeModel->instrument = $postData['instrument'];
    $tradeModel->limitPrice = $postData['limitPrice'];
    $tradeModel->orderCurrency = $postData['orderCurrency'];
    //$tradeModel->orderOrigin = $postData['orderOrigin'];
    $tradeModel->orderTermName = $postData['orderTermName'];
    $tradeModel->orderType = $postData['orderType'];
    $tradeModel->portfolioLabel = $postData['portfolioLabel'];
    $tradeModel->portfolioName = $postData['portfolioName'];
    $tradeModel->priceType = $postData['priceType'];
    $tradeModel->quantityRequested = $postData['quantityRequested'];
        
    $result = $tradeModel->getTradeOrderTotal();

    //Returns the response object to the user
    return $response->withStatus(200)
                    ->withJson($result, 200);
})->add(new Authentication());

/**
 * 
 */
$app->get('/api/findActiveTradeOrderTerms', function( $request, $response ) {
    
    //Returns the response object to the user
    return $response->withStatus(200)
                    ->withJson(TradeModel::findActiveTradeOrderTerms(), 200);
});

/**
 * 
 */
$app->post('/api/cancelTradeOrder', function( $request, $response ) {    
    $postData = $request->getParsedBody();    
    TradeModel::$tradeOrderId = $postData['id'];
    $result = "";
    
    if(isset(TradeModel::$tradeOrderId) && !empty(TradeModel::$tradeOrderId)){
        $result = TradeModel::cancelTradeOrder();
    }
    
    if(isset(TradeModel::$faultString) && !empty(TradeModel::$faultString)){
        return $response->withStatus(500)
                    ->withJson(TradeModel::$faultString, 500);
    }else{
        //Returns the response object to the user
        return $response->withStatus(200)
                    ->withJson($result, 200);
    }
    
})->add(new Authentication());