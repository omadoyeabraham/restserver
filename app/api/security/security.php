<?php

use models\SecurityModel;
use middleware\Authentication;

/**
 * This method returns a comprehensive list of Securities
 * If the data is cached it will fetch a local copy, if not it will
 * run gerSecurities
 * 
 * Cache is only stored for 20 seconds
 *
 * @author Olaleye Osunsanya
 *
 * 
 *  
 * */
$app->get('/api/getSecurity', function( $request, $response ) {
	$data = SecurityModel::getSecurities();
	
	return $response->withStatus(200)
                        ->withJson($data, 200);
	/**
    if (apcu_exists('securities')) { //checks if the cache copy exists
        return $response->withStatus(200)
                        ->withJson(apcu_fetch("securities"), 200);
    } else {        
        return $response->withStatus(404)
                        ->withJson([], 404);
    }**/
})->add(new Authentication());

/**
 * 
 */
$app->get('/api/findSecurityOverviewById/{id}', function($request, $response){
    //gets the name from the request object
    $id = $request->getAttribute('id');
    $data = SecurityModel::findSecurityOverviewById($id);
    
    return $response->withStatus(200)->withJson($data);
});

/**
 * 
 */
$app->get('/api/findSecurityOverviewByName/{name}', function($request, $response){
    //gets the name from the request object
    $name = $request->getAttribute('name');
    $data = SecurityModel::findSecurityOverviewByName($name);
    
    return $response->withStatus(200)->withJson($data);
});

$app->get('/api/fetchSecurity', function( $request, $response ) {
    $data = SecurityModel::getSecurities();
    apcu_store("securities", $data);
});


/**
 * This method returns the Id, name, label, and sector of all Securities
 * If the data is cached it will fetch a local copy, if not it will
 * run gerSecurities
 * 
 * Cache is only stored for 20 seconds
 *
 * @author Olaleye Osunsanya
 *
 * 
 *  
 * */
$app->get('/api/getSecurityNames', function( $request, $response ) {

    if (apcu_exists('securityNames')) {//checks if the cache copy exists
        return $response->withStatus(200)
                        ->withJson(apcu_fetch("securityNames"), 200);
    } else {        
        apcu_store("securityNames", SecurityModel::getSecurityNames());//Stores the data in cache until deleted by user
        return $response->withStatus(200)
                        ->withJson(apcu_fetch("securityNames"), 200);
    }
});

/**
 * This api gets the ASI five days data and stores the data in cache for a day
 * 
 * Write a micro service to run the API at 8am every day
 * Remember to remove the 86400 seconds after writing the micro service
 */
$app->get('/api/getAsiFiveDaysData', function( $request, $response ) {

    if (apcu_exists('ASIFiveDayData')) {
        return $response->withStatus(200)
                        ->withJson(apcu_fetch("ASIFiveDayData"), 200);
    } else {
        apcu_store("ASIFiveDayData", SecurityModel::getAsiFiveDaysData(), 86400);
        return $response->withStatus(200)
                        ->withJson(apcu_fetch("ASIFiveDayData"), 200);
    }
});

/**
 * 
 */
$app->get('/api/getMarketHighLight', function($request, $response) {
    
    if (apcu_exists('marketHighLight')) {
        return $response->withStatus(200)
                        ->withJson(apcu_fetch("marketHighLight"), 200);
    } else {
        apcu_store("marketHighLight", SecurityModel::getMarketHighLight(), 300);
        return $response->withStatus(200)
                        ->withJson(apcu_fetch("marketHighLight"), 200);
    }
});



$app->get('/api/getTopGainers', function($request, $response){
	//apcu_clear_cache();
	//SecurityModel::getTopGainers();
	die;

    if(apcu_exists('topGainers')){
        return $response->withStatus(200)
                ->withJson(apcu_fetch('topGainers'), 200);
    }else{
        apcu_store('topGainers', SecurityModel::getTopGainers(), 300);
        return $response->withStatus(200)
                        ->withJson(apcu_fetch("topGainers"), 200);
    }
});
