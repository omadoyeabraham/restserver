<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use models\EmailModel;

$app->post('/api/contactManager', function($request, $response){
    $postData = $request->getParsedBody();
    
    $subject = trim($postData['subject']);
    $message = trim($postData['message']);
    $email = trim($postData['email']);
    $userName = trim($postData['userName']);
    
    if(isset($subject) && !empty($subject) 
            && isset($message) && !empty($message) 
            && isset($email) && !empty($email) 
            && isset($userName) && !empty($userName)){
        $data = [
            'subject' => $subject,
            'email' => $email,
            'message' => $message,
            'userName' => $userName
        ];
        
		$result = EmailModel::sendMail($data, "contactmanager");

        if ($result) {
            //Returns the response object to the user
            return $response->withStatus(200)
                            ->withJson($result, 200);
        }
		
    }
    
});

