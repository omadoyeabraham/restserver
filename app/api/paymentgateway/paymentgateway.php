<?php

use models\PaymentGatewayModel;
use models\NIBBSModel;
use models\EmailModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$app->post('/api/getPaymentTransactionDetails', function($request, $response) {
    $paymentGateway = new PaymentGatewayModel();
    $postData = $request->getParsedBody();

    //product Id == cashaccount id in the database
    $paymentGateway->cashAccountId = (isset($postData['productId'])) ? trim($postData['productId']) : '';
    $paymentGateway->clientId = (isset($postData['clientId'])) ? trim($postData['clientId']) : '';
    $paymentGateway->productType = (isset($postData['productDescription'])) ? trim($postData['productDescription']) : '';
    $paymentGateway->productDescription = (isset($postData['productDescription'])) ? trim($postData['productDescription']) : '';
    $paymentGateway->amount = (isset($postData['amount'])) ? trim($postData['amount']) : '';
    $paymentGateway->companyName = (isset($postData['companyName'])) ? trim($postData['companyName']) : '';

    if ((int) $paymentGateway->amount < 50000) {
        //return $response->withStatus(500)
                        //->withJson(['status' => 'amount cannot be less than 50,000'], 500);
    }

    if ((int) $paymentGateway->amount > 20000000) {
        //return $response->withStatus(500)
                        //->withJson(['status' => 'amount cannot be more than 20,000,000'], 500);
    }

    if (!empty($paymentGateway->cashAccountId) && !empty($paymentGateway->clientId) 
            && !empty($paymentGateway->productType) && !empty($paymentGateway->productDescription) 
            && !empty($paymentGateway->amount) && !empty($paymentGateway->companyName)) {
		$result = $paymentGateway->getTransactionDetails();
        
        //Returns the response object to the user    
        if (!empty($result)) {
            return $response->withStatus(200)
                            ->withJson($result, 200);
        } else {
            return $response->withStatus(500);
        }
    }else{
        return $response->withStatus(500);
    }
});

/**
  $app->post('/api/postTransaction', function($request, $response) {
  $paymentGateway = new PaymentGatewayModel();
  $postData = $request->getParsedBody();

  $paymentGateway->cashAccountId = $postData['productId']; //product Id == cashaccount id in the database
  $paymentGateway->clientId = $postData['clientId'];
  $paymentGateway->productType = $postData['productDescription'];
  $paymentGateway->productDescription = $postData['productDescription'];
  $paymentGateway->amount = $postData['amount'];
  $paymentGateway->companyName = $postData['companyName'];

  $result = $paymentGateway->postTransaction();
  //Returns the response object to the user
  if (!empty($result)) {
  return $response->withStatus(200)
  ->withJson($result, 200);
  } else {
  return $response->withStatus(500);
  }
  });

 * */
/**
 * 
 */
$app->post('/api/validateTransaction', function($request, $response) {
    $postData = $request->getParsedBody();
    $transactionId = urlencode($postData['transaction_id']);
    $cpayRef = urlencode($postData['cpay_ref']);
    $merchantId = urlencode($postData['merchant_id']);
    
    $paymentGateway = new PaymentGatewayModel();

    //$result = NIBBSModel::postDataBackToNIBBS($transactionId, $cpayRef, $merchantId);
    $result = $paymentGateway->postTransaction($transactionId, $cpayRef, $merchantId);	

    if ($result) {
        return $response->withRedirect('https://portal.cardinalstone.com/broker/desktop/#/fund-account?s='.substr(md5('0000'), 0, 6));
    }else{
		return $response->withRedirect('https://portal.cardinalstone.com/broker/desktop/#/fund-account?s=1111');
	}
});

$app->get('/api/validateTransaction', function($request, $response) {
    $transactionId = $request->getParam('transaction_id');
    $cpayRef = $request->getParam('cpay_ref');
    $merchantId = $request->getParam('merchant_id');
    
    $paymentGateway = new PaymentGatewayModel();
    
    //$result = NIBBSModel::postDataBackToNIBBS($transactionId, $cpayRef, $merchantId);
    $result = $paymentGateway->postTransaction($transactionId, $cpayRef, $merchantId);  
   
    if ($result) {
        return $response->withRedirect('https://portal.cardinalstone.com/broker/staging/#/fund-account?s='.substr(md5('0000'), 0, 6));
    }else{
    return $response->withRedirect('https://portal.cardinalstone.com/broker/staging/#/fund-account?s=1111');
  }
});


$app->post('/api/paymentgateway', function($request, $response) {
    
});
