<?php
/**
 * Description of Authentication
 *
 * @author Olaleye
 */
namespace middleware;

use models\CustomerModel as customerModel;

class Authentication {

    /**
     * Authentication middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next) {
        $auth = $request->getHeader('Authorization');
        $token = $auth[0];
        $customer = new customerModel();        
        if(!$customer->authenticate($token)){
            return $response->withStatus(401);
        }
        
        $response = $next($request, $response);
        return $response;
    }

}
