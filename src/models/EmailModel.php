<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace models;

use PHPMailer\PHPMailer\PHPMailer;

/**
 * Description of EmailModel
 *
 * @author Olaleye
 */
class EmailModel {

    //put your code here
    //private $mail;
    private static $SMTPDebug = 0; // Enable verbose debug output
    private static $host = "smtp.office365.com"; // Specify main and backup SMTP servers
    private static $SMTPAuth = TRUE; // Enable SMTP authentication
    private static $username = "info@cardinalstone.com"; // SMTP username
    private static $password = "csp_1234"; // SMTP password
    private static $SMTPSecure = "tls"; // Enable TLS encryption, `ssl` also accepted
    private static $port = "587"; // TCP port to connect to
    private static $fromEmail = "crd@cardinalstone.com";
    private static $fromName = "CardinalStone";
    private static $message = "";
    private static $mailTo = "mailto:crd@cardinalstone.com";
    private static $website = "http://www.cardinalstone.com";

    public function __construct() {
        
    }

    public static function sendMail($data, $flag = NULL) {
        $mail = new PHPMailer(); // Passing `true` enables exceptions

        try {
            $mail->SMTPDebug = self::$SMTPDebug;
            $mail->isSMTP();
            $mail->Host = self::$host;
            $mail->SMTPAuth = self::$SMTPAuth;
            $mail->Username = self::$username;
            $mail->Password = self::$password;
            $mail->SMTPSecure = self::$SMTPSecure;
            $mail->Port = self::$port;

            //RECIPIENTS
            $mail->setFrom(self::$fromEmail, self::$fromName);
            
            //$mail->addBCC("crd@cardinalstone.com");
            //$mail->addBCC("fincon@cardinalstone.com");
            //Content
            // Set email format to HTML
            $mail->isHTML(true);

            if ($flag === "watchList") {
                $mail->addAddress($data['userEmail'], $data['userName']);
                //echo 'WatchList Email';die;
                $mail->Subject = 'CardinalStone Watchlist Notification';
                $mail->Body = self::getWatchListMessage($data);
                //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                $mail->send();
            }


            if ($flag === "resetPassword") {
                $mail->addAddress($data['userEmail'], $data['userName']);
                //echo 'WatchList Email';die;
                $mail->Subject = 'CardinalStone Password Reset';
                $mail->Body = self::getPasswordResetMessage($data);
                //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                $mail->send();
            }  
            
            if($flag === NULL) {
                $mail->addAddress($data['userEmail'], $data['userName']);
                $mail->Subject = 'CardinalStone Transaction Alert';
                $mail->Body = self::getMessage($data);
                //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                $mail->send();
            }
            
            if($flag === "contactmanager"){
                $mail->addAddress($data['email'], 'Client Portal - Contact Manager');
                $mail->Subject = $data['userName'] ." - ". $data['subject'];
                $mail->Body = $data['message'];
                //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                $mail->send();
            }


            //echo 'Message has been sent';
        } catch (Exception $e) {
            //echo 'Message could not be sent.';
            //echo 'Mailer Error: ' . $mail->ErrorInfo;
        }
    }

    private static function getMessage($data = []) {
        $amount = number_format($data['amount'], 2);
        self::$message = "";
        self::$message .= "<!DOCTYPE html><html lang='en-US'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'/></head><body>";
        self::$message .= "<div style='font-family: Calibri, sans-serif;'>";
        self::$message .= "<p> Dear " . $data['userName'] . "</p>";
        self::$message .= "<p> Please be informed that you have successfully funded
                            your CardinalStone account with the sum of &#8358;" . $amount
                . ". Kindly find the transaction details below: </p>";
        self::$message .= "<ul>";
        self::$message .= "<li>Transaction Date: " . $data['transactionDate'] . "</li>";
        self::$message .= "<li>Transaction Reference Number: " . $data['transactionReference'] . "</li>";
        self::$message .= "<li>Amount funded: &#8358;" . $amount . "</li>";
        self::$message .= "<li>Transaction Message: " . $data['responseMessage'] . "</li>";
        self::$message .= "</ul>";
        self::$message .= "<p> Regards,<br><br>
                            Asset Management Operations <br>
                            CardinalStone Partners Limited <br>
                            5 Okotie Eboh Street<br>
                            South-West Ikoyi<br>
                            Lagos, Nigeria<br>
                            Tel: +234 (1) 7100433-4<br>
                            Email: <a href=" . self::$mailTo . "> crd@cardinalstone.com</a><br>
                            Website: <a href=" . self::$website . "> www.cardinalstone.com</a>​</p>";
        self::$message .= "</div>";
        self::$message .= "</body>";
        self::$message .= "</html>";

        return self::$message;
    }
    
    private static function getWatchListMessage($data = []) {
        //$amount = number_format($data['amount'], 2);
        self::$message = "";
        self::$message .= "<!DOCTYPE html><html lang='en-US'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'/></head><body>";
        self::$message .= "<div style='font-family: Calibri, sans-serif;'>";
        self::$message .= "<p> Dear " . $data['userName'] . "</p>";
        self::$message .= "<p> Please be informed that matches have been found in your watchlist for the following:</p>";
        self::$message .= "<table cellspacing='0' cellpadding='5' border='1' style='font-family: Calibri, 'Segoe UI', sans-serif; border-color: #bfbfbf !important;'>";
        self::$message .= "<thead style='color: #fff !important; background-color: #1a2155 !important;'>";
        self::$message .= "<th>S/N</th>";
        self::$message .= "<th>Security</th>";
        self::$message .= "<th>Current Price</th>";
        self::$message .= "<th>Condition</th>";
        self::$message .= "<th>Watchlist Price</th>";
        self::$message .= "</thead>";
        self::$message .= "<tbody>";
        self::$message .= "<tr>";
        self::$message .= "<td align='right'>1</td>";
        self::$message .= "<td>" . $data['security'] . "</td>";
        self::$message .= "<td align='right'>&#8358;" . number_format($data['currentPrice'], 2) . "</td>";
        self::$message .= "<td align='center'>" . $data['condition'] . "</td>";
        self::$message .= "<td align='right'>&#8358;" . number_format($data['watchListPrice'], 2) . "</td>";
        self::$message .= "</tr>";
        self::$message .= "</tbody>";
        self::$message .= "</table>";
        
        self::$message .= "<p> Regards,<br><br>
                            Asset Management Operations <br>
                            CardinalStone Partners Limited <br>
                            5 Okotie Eboh Street<br>
                            South-West Ikoyi<br>
                            Lagos, Nigeria<br>
                            Tel: +234 (1) 7100433-4<br>
                            Email: <a href='info@cardinalstone.com'> info@cardinalstone.com</a><br>
                            Website: <a href=" . self::$website . "> www.cardinalstone.com</a>​</p>";
        self::$message .= "</div>";
        self::$message .= "</body>";
        self::$message .= "</html>";

        return self::$message;
    }

    private static function getPasswordResetMessage($data = []) {
        //self::$message .= "click here "."<a href=" . $data['link'] . ">Reset Link</a>​";

         self::$message = "";
        self::$message .= "<!DOCTYPE html><html lang='en-US'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'/></head><body>";
        self::$message .= "<div style='font-family: Calibri, sans-serif;'>";
        self::$message .= "<p> Dear " . $data['userLabel'] . ",</p>";
        self::$message .= "<p> Kindly click on this <a href=" . $data["link"] . ">link</a>​ to reset your password as requested. Please note that this link will expire within 2 hours.  </p>";
        self::$message .= "<p>If clicking the link above doesn't work, please copy and paste this url into new browser window instead. </p>";
        self::$message .= $data["link"];
        self::$message .= "<br><br><small>** If you've received this mail in error, it's likely that another user entered your email address by mistake while trying to reset a password. If you didn't initiate the request, you don't need to take any further action and can safely disregard this email.  </small>";
        self::$message .= "<p> Regards,<br><br>
                            Asset Management Operations <br>
                            CardinalStone Partners Limited <br>
                            5 Okotie Eboh Street<br>
                            South-West Ikoyi<br>
                            Lagos, Nigeria<br>
                            Tel: +234 (1) 7100433-4<br>
                            Email: <a href=" . self::$mailTo . "> crd@cardinalstone.com</a><br>
                            Website: <a href=" . self::$website . "> www.cardinalstone.com</a>​</p>";
        self::$message .= "</div>";
        self::$message .= "</body>";
        self::$message .= "</html>";


        return self::$message;
    }

}
