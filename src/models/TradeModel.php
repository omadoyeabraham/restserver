<?php

/* --------------------------------------------------------------
  # TradeModel added By Olaleye Osunsanya, Date: August 22, 2017

  This class handles Trades


  NOTE: Because my code looks beautiful does not mean they are good.
  Word for the wise: Silence is golden!
  WARNING: My comments might hurt your feelings!
  -------------------------------------------------------------- */

namespace models;

use models\WebServiceModel as webserviceModel;

/**
 * Description of TradeModel
 *
 * @author Olaleye
 */
class TradeModel {
    /* --------------------------------------------------------------
      # Variables added By Olaleye Osunsanya, Date: August 24, 2017
      -------------------------------------------------------------- */

    public $instrument;
    public $limitPrice;
    public $orderCurrency;
    public $orderOrigin;
    public $orderTermName;
    public $orderType;
    public $portfolioLabel;
    public $portfolioName;
    public $priceType;
    public $quantityRequested;
    public static $tradeOrderId;
	public static $faultString;

    /**
     * Default Constructor
     */
    public function __construct() {
        $this->instrument = "";
        $this->limitPrice = "";
        $this->orderCurrency = "";
        $this->orderOrigin = "";
        $this->orderTermName = "";
        $this->orderType = "";
        $this->portfolioLabel = "";
        $this->portfolioName = "";
        $this->priceType = "";
        $this->quantityRequested = "";
    }

    /**
     * 
     * @return type
     */
    public function createTradeOrder() {
        //calls Zanibal's soap method "createTradeOrder"
        $result = webserviceModel::getWebServiceConnection()->createTradeOrder(
                [
                    "instrument" => $this->instrument,
                    "limitPrice" => $this->limitPrice,
                    "orderCurrency" => $this->orderCurrency,
                    "orderOrigin" => $this->orderOrigin,
                    "orderTermName" => $this->orderTermName,
                    "orderType" => $this->orderType,
                    "portfolioLabel" => $this->portfolioLabel,
                    "portfolioName" => $this->portfolioName,
                    "priceType" => $this->priceType,
                    "quantityRequested" => $this->quantityRequested
                ]
        );

        if (is_soap_fault($result)) {
            return $result->faultstring;
        } else {
            return $result;
        }
    }

    public function getTradeOrderTotal() {
        //calls Zanibal's soap method "getTradeOrderTotal"
        $result = webserviceModel::getWebServiceConnection()->getTradeOrderTotal(
                [
                    "instrument" => $this->instrument,
                    "limitPrice" => $this->limitPrice,
                    "orderCurrency" => $this->orderCurrency,
                    "orderOrigin" => $this->orderOrigin,
                    "orderTermName" => $this->orderTermName,
                    "orderType" => $this->orderType,
                    "portfolioLabel" => $this->portfolioLabel,
                    "portfolioName" => $this->portfolioName,
                    "priceType" => $this->priceType,
                    "quantityRequested" => $this->quantityRequested
                ]
        );

        if (is_soap_fault($result)) {
            $data = $this->getFaultErrorMessage($result->faultstring);
            return $data;
        } else {
            return $result;
        }
    }

    /**
     * 
     * @return type
     */
    public static function findActiveTradeOrderTerms() {
        $result = webserviceModel::getWebServiceConnection()->findActiveTradeOrderTerms();

        if (is_soap_fault($result)) {
            return $result->faultstring;
        } else {
            return $result;
        }
    }

    /**
     * 
     * @return type
     */
    public static function cancelTradeOrder() {
        $result = webserviceModel::getWebServiceConnection()
                ->cancelTradeOrder(TradeModel::$tradeOrderId);

        if (is_soap_fault($result)) {
            self::$faultString = "";
            self::$faultString = $result->faultstring;
            return self::$faultString;
        } else {
            return $result;
        }
    }
	
	/**
     * 
     * @param type $data
     * @return type
     */
    private function getFaultErrorMessage($data) {
        $result = explode("|", $data);
        if(strpos($result[0], "BUY") != FALSE){
            return [
                'status' => 'INSUFFICIENT FUNDS FOR BUY ORDER',
                'availableBalance' => $result[1],
                'totalConsideration' => $result[3],
                'requestedQuantity' => $result[4],
                'contractFees' => $result[5]
            ];
        }
        
        if(strpos($result[0], "SELL") != FALSE){
            return ['status' => 'INSUFFICIENT SHARES FOR SELL ORDER, PLEASE CHECK OUTSTANDING MANDATES'];
        }
    }

}
