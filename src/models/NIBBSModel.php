<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace models;

use models\DatabaseModel as dbModel;
use models\EmailModel;

/**
 * Description of NIBBSModel
 *
 * @author Olaleye
 */
class NIBBSModel {

    private static $responseCode = [
        "000" => "Approved by Financial Institution",
        "001" => "Account error, please contact your bank",
        "002" => "The amount requested is above the limit permitted by your bank, please contact your bank",
        "003" => "The amount requested is too low"
    ];
//    public static $nibbsURL = "https://centralpay.nibss-plc.com.ng/CentralPayPlus/pay";
    
    /**
    * MerchantId, SecretKey and RequestURL for NIbbs staging environment
    */
    // public static $merchantId = "NIBSS0000000104"; //for test
    // private static $secretKey = "7BAD246133F6659B451C39D64955D37E"; //for test
    // private static $requestURL = "https://staging.nibss-plc.com.ng/CentralPayPlus/merchantTransQueryJSON"; //for test

    public static $merchantId = "NIBSS0000004020";
    private static $secretKey = "E353632F8071201639F5A81A5BA67406";    
    private static $requestURL = "https://centralpay.nibss-plc.com.ng/CentralPayPlus/merchantTransQueryJSON";    
    private static $hashAlgorithm = 'sha256';
//    private static $bankCode = "014";
    public static $consideration = 100;
    public static $currency = "566";
    public static $responseURL = "https://restserver2.cardinalstone.com/api/validateTransaction";
    public static $status = "pending";
    public $transactionId;
    public $cpayRef;
    public $dataToHash;
    private $hashedData;
    public $clientId;
    public $cashAccountId;
    public $productType;
    public $amount;
    public $transactionDate;
    public $productDescription;
    public $total;
    public $companyName;

    /**
     * Default constructor
     */
    public function __construct() {
        $this->transactionId = "";
        $this->cpayRef = "";
        $this->dataToHash = "";
        $this->hashedData = "";
        $this->clientId = "";
        $this->cashAccountId = "";
        $this->productType = "";
        $this->amount = "";
        $this->transactionDate = "'" . date('Y-m-d H:i:s') . "'";
        $this->productDescription = "";
        $this->total = 0;
        $this->hashedData = "";
        $this->companyName = "";
    }

    public function nibbsCallback() {
        $this->dataToHash = $this->transactionId
                . $this->cpayRef
                . NIBBSModel::$merchantId
                . NIBBSModel::$secretKey;

        if (isset($this->dataToHash) && !empty($this->dataToHash)) {
            $this->hashedData = hash(NIBBSModel::$hashAlgorithm, $this->dataToHash);
            $this->postDataBackToNIBBS();
        }
    }

    /**
     * 
     * @return type
     */
    public static function postDataBackToNIBBS($transactionId, $cpayRef, $merchantId) {
        $fields = [
            'transaction_id' => $transactionId,
            'cpay_ref' => $cpayRef,
            'merchant_id' => $merchantId,
            'hash' => hash(self::$hashAlgorithm, $transactionId . $cpayRef . $merchantId . self::$secretKey)
        ];

        $fields_string = http_build_query($fields);

        $curl = curl_init(NIBBSModel::$requestURL);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $curl_response = curl_exec($curl);

        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            return [];
        }
        curl_close($curl);
        $data = json_decode($curl_response);
        $result = FALSE;
        if (($data->ResponseCode === '000')) {
            $result = self::updateTransaction($data);
            $dbResult = self::getTransactionData($data);
            $customer = apcu_fetch($dbResult['client_id']);
            EmailModel::sendMail([
                'userName' => $customer->label,
                'userEmail' => $customer->emailAddress1,
                'amount' => $dbResult['amount'],
                'transactionDate' => $dbResult['transaction_date'],
                'transactionReference' => $dbResult['transaction_ref'],
                'responseMessage' => 'Approved by Financial Institution'
            ]);
            return $result;
        }else{
            return FALSE;
        }
    }

    public function sendDataToNIBBS() {
        
    }

    /**
     * 
     * @param type $transactionId
     * @return type
     */
    public function isTransactionIdUnique($transactionId) {
        $query = "SELECT transaction_ref FROM client_portal.nibss_payment_transactions
                    WHERE transaction_ref LIKE " . "'" . "%" . $transactionId . "%" . "'" . " LIMIT 1;";
        $data = dbModel::getClientPortalConnection()->query($query);
        return ($data->num_rows === 0) ? TRUE : FALSE;
    }

    /**
     * 
     * @return type
     */
    private function generateHash() {
        $this->total = ($this->amount + self::$consideration) * 100;
        $data = self::$merchantId
                . $this->cashAccountId
                . $this->productDescription
                . $this->total
                . self::$currency
                . $this->transactionId
                . self::$responseURL
                . self::$secretKey;
        return hash(self::$hashAlgorithm, $data);
    }

    /**
     * 
     * @return type
     */
    public function createTransaction() {
        $this->hashedData = $this->generateHash();
        $query = "INSERT INTO client_portal.nibss_payment_transactions(
                        `transaction_ref`, `status`, `client_id`, `cash_account_id`,
                        `product_type`, `amount`, `transaction_date`, `currency_code`,
                        `product_description`, `hash`, `createdDate`, `companyName`) 
                    VALUES(" . "'" . $this->transactionId . "'" . ", " . "'" . NIBBSModel::$status . "'" . ",
                        $this->clientId, " . "'" . $this->cashAccountId . "'" . ", 
                        " . "'" . $this->productType . "'" . ", $this->amount,
                        $this->transactionDate, " . "'" . NIBBSModel::$currency . "'" . ",
                        " . "'" . $this->productDescription . "'" . ","
                . "'" . $this->hashedData . "'" . ", $this->transactionDate, " . "'" . $this->companyName . "'" . ");";

        dbModel::getClientPortalConnection()->query($query);
        $insertId = mysqli_insert_id(dbModel::getClientPortalConnection());

        if ($insertId > 0) {
            return [
                'transactionId' => $this->transactionId,
                'merchantId' => self::$merchantId,
                'currency' => self::$currency,
                'responseURL' => self::$responseURL,
                'hash' => $this->hashedData,
                'charge' => self::$consideration,
                'amountInKobo' => ($this->amount + self::$consideration) * 100
            ];
        } else {
            return [];
        }
    }

    private static function updateTransaction($data) {
        $query = "UPDATE client_portal.nibss_payment_transactions
                    SET `status` = 'successful',
                        `cpayref` = " . "'" . $data->CPAYRef . "'" . ",
                        `transaction_date` = " . "'" . $data->TransDate . "'" . ",
                        `modifiedDate` = " . "'" . date('Y-m-d H:i:s') . "'" . "
                    WHERE `transaction_ref` = $data->TransactionId AND `cash_account_id` = $data->ProductId;";
        dbModel::getClientPortalConnection()->query($query);
        $affectedRows = mysqli_affected_rows(dbModel::getClientPortalConnection());

        return ($affectedRows > 0) ? TRUE : FALSE;
    }

    private static function getTransactionData($data) {
        $query = "SELECT client_id, amount, transaction_ref, product_description,
                    transaction_date
                    FROM client_portal.nibss_payment_transactions
                    WHERE `transaction_ref` = $data->TransactionId
                    AND `cash_account_id` = $data->ProductId
                        LIMIT 1;";

        $result = dbModel::getClientPortalConnection()->query($query);
        if ($result->num_rows != 0) {
            return $result->fetch_assoc();
        } else {
            return [];
        }
    }

}
