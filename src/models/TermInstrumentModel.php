<?php

/* --------------------------------------------------------------
  # DashboardModel added By Olaleye Osunsanya, Date: August 21, 2017

  This class handles everything on the Dashboard of the Trade Portal


  NOTE: Because my code looks beautiful does not mean they are good.
  Word for the wise: Silence is golden!
  WARNING: My comments might hurt your feelings!
  -------------------------------------------------------------- */

/**
 * Description of DashboardModel
 *
 * @author Olaleye Osunsanya
 */

namespace models;

use models\WebServiceModel as webserviceModel;
use models\DatabaseModel as dbModel;
use models\TBillModel;

/**
 * Description of TermInstrumentModel
 *
 * @author Olaleye
 */
class TermInstrumentModel {
    /* --------------------------------------------------------------
      # Variables added By Olaleye Osunsanya, Date: August 21, 2017
      -------------------------------------------------------------- */

    private $id;
    private $startRecord;
    private $numberOfRecords;
    private $arg;
    private $status;
    private $startDate;
    private $endDate;
    private $termInstruments = [];
    private $NGNTermInstruments = [];
    private $NGNSMATermInstruments = [];
    private $USDTermInstruments = [];
    private $USDTermInstrumentNames = [];
    private $fiAcctId;
    private $cashAccountModel;
    private $tBillsModel;

    /**
     * Default constructor
     */
    public function __construct(CashAccountModel $cashAccountModel) {
        $this->id = "";
        $this->startRecord = 0;
        $this->numberOfRecords = 100;
        $this->arg = "";
        $this->status = "RUNNING";
        $this->startDate = "";
        $this->endDate = "";
        $this->termInstruments = [];
        $this->NGNTermInstruments = [];
        $this->NGNSMATermInstruments = [];
        $this->USDTermInstruments = [];
        $this->USDTermInstrumentNames = [];
        $this->fiAcctId = "";
        $this->cashAccountModel = $cashAccountModel;
        $this->tBillsModel = new TBillModel();
    }

    /**
     * 
     * @param type $id
     * @param type $startRecord
     * @param type $numberOfRecords
     * @param type $arg
     * @param type $status
     * @param type $startDate
     * @param type $endDate
     * @return array
     */
    function findCustomerTermInstruments($id, $status = NULL, $startDate = NULL, $endDate = NULL) {

        $this->id = $id;
        $this->startRecord = 0;
        $this->numberOfRecords = 100;
        $this->arg = "";
        //$this->status = ($status === NULL) ? "RUNNING" : $status;
        $this->status = ($status === NULL) ? "" : $status;
        $this->startDate = ($startDate === NULL) ? "" : $startDate;
        $this->endDate = ($endDate === NULL) ? "" : $endDate;

        //calls Zanibal's soap method "findCustomerById"
        $termInstruments = webserviceModel::getWebServiceConnection()
                ->findCustomerTermInstruments(
                        $this->id, $this->startRecord, $this->numberOfRecords, $this->arg, $this->status, $this->startDate, $this->endDate                
        );

        $termInstruments_array = [];

        /**
         *
         * Checks that the termInstruments returned is an array 
         * if not it returns an array
         * 
         * */
        if (isset($termInstruments->item) && is_array($termInstruments->item)) {
            $termInstruments_array = $termInstruments->item;
        }

        if (isset($termInstruments->item) && !is_array($termInstruments->item)) {
            $termInstruments_array = [$termInstruments->item];
        }

        // && !empty($termInstruments_array)
        if (isset($termInstruments_array) ) {

            //Cache this for a day
            $this->termInstruments['NGN'] = $this->getNGNTermInstruments($termInstruments_array);
            $this->termInstruments['NGNSMA'] = $this->getNGNSMATermInstruments($termInstruments_array);
            $this->termInstruments['USD'] = $this->getUSDTermInstruments($termInstruments_array);
            $this->termInstruments['USDNGNRATES'] = $this->getNairaRates($termInstruments_array);


            //Implement APCU Cache for TBills
            $this->termInstruments['TBills'] = $this->tBillsModel->findTBills($id, $this->status);
        }

        //var_dump($termInstruments_array);
        //die;

        return $this->termInstruments;
    }

    /**
     * 
     * @param type $termInstruments_array
     * @return array
     */
    function getNGNTermInstruments($termInstruments_array = []) {

        if (!empty($termInstruments_array)) {

            foreach ($termInstruments_array as $key => $value) {
                if ($termInstruments_array[$key]->currency === NAIRA &&
                        ( strpos($termInstruments_array[$key]->instrumentTypeLabel, "SMA") === false )
                ) {
                    array_push($this->NGNTermInstruments, $termInstruments_array[$key]);
                }
            }
            return $this->NGNTermInstruments;
        }

        return [];
    }

    /**
     * 
     * @param type $termInstruments_array
     * @return array
     */
    function getNGNSMATermInstruments($termInstruments_array = []) {

        if (!empty($termInstruments_array)) {
            foreach ($termInstruments_array as $key => $value) {
                if ($termInstruments_array[$key]->currency === NAIRA &&
                        ( strpos($termInstruments_array[$key]->instrumentTypeLabel, "SMA") !== false )
                ) {
                    array_push($this->NGNSMATermInstruments, $termInstruments_array[$key]);
                }
            }
            return $this->NGNSMATermInstruments;
        }

        return [];
    }

    /**
     * 
     * @param type $termInstruments_array
     * @return array
     */
    function getUSDTermInstruments($termInstruments_array = []) {

        if (!empty($termInstruments_array)) {
            foreach ($termInstruments_array as $key => $value) {
                if ($termInstruments_array[$key]->currency === DOLLAR) {
                    array_push($this->USDTermInstruments, $termInstruments_array[$key]);
                }
            }
            return $this->USDTermInstruments;
        }

        return [];
    }

    /**
     * 
     * @param type $termInstruments_array
     * @return array
     */
    function getUSDTermInstrumentNames($termInstruments_array = []) {

        if (!empty($termInstruments_array)) {
            foreach ($termInstruments_array as $key => $value) {
                if ($termInstruments_array[$key]->currency === DOLLAR) {
                    array_push($this->USDTermInstrumentNames, $termInstruments_array[$key]->name);
                }
            }
            return $this->USDTermInstrumentNames; //
        }

        return [];
    }

    /**
     * 
     * @param type $termInstruments_array
     * @return type
     */
    function getNairaRates($termInstruments_array = []) {
        $this->USDTermInstrumentNames = $this->getUSDTermInstrumentNames($termInstruments_array);

        if (!empty($termInstruments_array)) {
            $this->id = $termInstruments_array[0]->customerId;
            $this->fiAcctId = $this->cashAccountModel->findCustomerNariaRateCashAccountId($this->id);
        }

        if (!empty($this->USDTermInstrumentNames) && !empty($this->fiAcctId)) {
            $termInstrumentsNames = join(', ', $this->USDTermInstrumentNames);
            $query = "SELECT DISTINCT voucherNo, currencyrate, reportcurrrate, 
                    entryType, createdDttm
                    FROM zebroker_prod.filedger
                    WHERE fiacct_id = $this->fiAcctId
                    AND isReversal = 0
                    AND active = 1
                    AND voucherNo IN ($termInstrumentsNames);";

            $result = dbModel::getMySQLiDBConnection()->query($query);

            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        } else {
          return [];
        }
    }

}
