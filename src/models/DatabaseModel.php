<?php

/* --------------------------------------------------------------
  # DatabaseModel added By Olaleye Osunsanya, Date: August 22, 2017

  This class handles the database connection. I did not use a Singleton
  because of obvious reasons.


  NOTE: Because my code looks beautiful does not mean they are good.
  Word for the wise: Silence is golden!
  WARNING: My comments might hurt your feelings!
  -------------------------------------------------------------- */

namespace models;

use mysqli;

/**
 * Description of DatabaseModel
 *
 * @author Olaleye
 */
class DatabaseModel {
    /* --------------------------------------------------------------
      # Variables added By Olaleye Osunsanya, Date: August 22, 2017
     * 
     * This variables were declared static to avoid multiple connections
     * It makes it work like a Singleton
     * 
     * I believe it is a better implementation than the Singleton pattern
     * Please do not quote me
      -------------------------------------------------------------- */

    private static $host = "zanibal.cardinalstone.com";
    private static $user = "zanibal";
    private static $password = "zanibal";
    private static $dbName = "zebroker_prod";
    private static $connection;
    private static $portal_connection;

    /**
     * Default constructor
     */
    public function __construct() {
        
    }

    /**
     * 
     * @return type
     */
    public static function getMySQLiDBConnection() {

        if (!isset(DatabaseModel::$connection)) {
            DatabaseModel::$connection = new mysqli
                    (
                    DatabaseModel::$host, DatabaseModel::$user, DatabaseModel::$password, DatabaseModel::$dbName
            );
        }

        return DatabaseModel::$connection;
    }

    /**
     * 
     * @return type
     */
    public static function getClientPortalConnection() {
        if (!isset(DatabaseModel::$portal_connection)) {
            DatabaseModel::$portal_connection = new mysqli(
                    "132.148.17.11", "cardinalstone_remote", "Guardian365", "client_portal"
            );
        }

        return DatabaseModel::$portal_connection;
    }

}
