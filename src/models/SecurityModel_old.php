<?php

/* --------------------------------------------------------------
  # SecurityModel added By Olaleye Osunsanya, Date: August 24, 2017

  This class handles a Securities. I have no idea why it is called
  securities instead of stocks lol


  NOTE: Because my code looks beautiful does not mean they are good.
  Word for the wise: Silence is golden!
  WARNING: My comments might hurt your feelings!
  -------------------------------------------------------------- */

namespace models;

use models\DatabaseModel as dbModel;
use models\WebServiceModel as webserviceModel;

/**
 * Description of SecurityModel
 *
 * @author Olaleye
 */
class SecurityModel {
    /* --------------------------------------------------------------
      # Variables added By Olaleye Osunsanya, Date: August 24, 2017
      -------------------------------------------------------------- */

    private static $securities = [];
    private static $securityNames = [];
    private static $unWantedSecuritiesId = [
        "348", "406", "350", "351", "352", "353", "354",
        "358", "364", "365", "377", "378", "382", "383",
        "385", "386", "387", "388", "389", "392", "395", "399", "400", "201",
        "347", "403"
    ];
    private static $fiveDayASIData = [];
    private static $exchange = "NSE";
    private static $gainersRecordType = "PRICE_PERCENTAGE_GAINERS";
    private static $loosersRecordType = "PRICE_PERCENTAGE_LOOSERS";
    private static $asiURL = "https://mds.zanibal.com/mds/rest/api/v1/research/get-security-overview/symbol?x=NSE&s=ASI";
    private static $securityMDSURL = "https://mds.zanibal.com/mds/rest/api/v1/research/get-security-overview/symbol?x=NSE&s=";

    /**
     * Default Constructor
     */
    public function __construct() {
        
    }

    /**
     * Gets all the securities and filters the unwanted ones
     * 
     * @return array
     */
    public static function getSecurities() {
        $query = "SELECT s.name, s.id, s.label, s.sector, s.currentValueDate,
					sp.previousClose, s.instr_type,
					sp.closingPrice, sp.openingPrice,
					sp.highPrice, noDeals, 
					sp.lowPrice, sp.refPrice AS currentPrice,
					sp.lastTradePrice,
					sp.quantityTraded, sp.valueTraded, sp.offerPrice, 
					sp.bidPrice, sp.bestOfferQty, sp.bestBidQty, sp.lastTradePrice
					FROM zebroker_prod.security s
					INNER JOIN (
					SELECT security_id, refPrice, lastTradePrice, priceDttm, previousClose, closingPrice,
						openingPrice, highPrice, noDeals, lowPrice, quantityTraded, valueTraded,
						offerPrice, bidPrice, bestOfferQty, bestBidQty
					FROM zebroker_prod.securityprice
					ORDER BY priceDttm DESC LIMIT 300
					) sp

					ON s.id = sp.security_id
					WHERE s.active = 1
					AND sp.refPrice >= 0.5 
					AND ( s.name IS NOT NULL AND s.name != '' )
					GROUP BY s.name;";

        $result = dbModel::getMySQLiDBConnection()->query($query);

        if ($result->num_rows != 0) {
            while ($row = $result->fetch_assoc()) {
                SecurityModel::filterSecurities($row);
            }
            return SecurityModel::$securities;
        } else {
            return [];
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public static function findSecurityOverviewById($id){
        $result = webserviceModel::getWebServiceConnection()
                ->findSecurityOverviewById( $id);

        if (is_soap_fault($result)) {
            return $result->faultstring;
        } else {
            return $result;
        }
    }

        /**
     * 
     * @param type $name
     * @return type
     */
    public static function findSecurityOverviewByName($name) {
        $curl = curl_init(SecurityModel::$securityMDSURL . $name);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $curl_response = curl_exec($curl);

        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            return [];
            //return $info;
        }
        curl_close($curl);
        $data = json_decode($curl_response);
        return $data;
    }

    /**
     * 
     * @return type
     */
    public static function getSecurityNames() {
        $query = "SELECT s.id, s.name, s.label, s.sector
                    FROM zebroker_prod.security s
                    WHERE s.active = 1
                    AND ( s.name IS NOT NULL AND s.name != '' )
                    GROUP BY s.name;";
        $result = dbModel::getMySQLiDBConnection()->query($query);

        if ($result->num_rows != 0) {
            while ($row = $result->fetch_assoc()) {
                if (!in_array($row['id'], SecurityModel::$unWantedSecuritiesId, TRUE)) {
                    array_push(SecurityModel::$securityNames, $row);
                }
            }
            return SecurityModel::$securityNames;
        } else {
            return [];
        }
    }

    /**
     * 
     * @return array
     */
    public static function getAsiFiveDaysData() {
        $query = "SELECT closingPrice, createdDttm
                 FROM zebroker_prod.securityprice
                 WHERE security_id = 331
                 ORDER BY id DESC
                 LIMIT 5;";

        $result = dbModel::getMySQLiDBConnection()->query($query);

        if ($result->num_rows != 0) {
            while ($row = $result->fetch_assoc()) {
                array_push(SecurityModel::$fiveDayASIData, $row);
            }
            return SecurityModel::$fiveDayASIData;
        } else {
            return [];
        }
    }

    /**
     * 
     * @return type
     */
    public static function getTopGainers() {
        //calls Zanibal's soap method "getMarketLeaders"
        $result = webserviceModel::getWebServiceConnection()->getMarketLeaders(
                SecurityModel::$exchange, SecurityModel::$gainersRecordType, 0, 5
        );
        
        if (is_soap_fault($result)) {
            return $result->faultstring;
        } else {
			$jsonData = json_encode($result, JSON_PARTIAL_OUTPUT_ON_ERROR);
            return json_decode($jsonData);
        }
    }

    /**
     * 
     * @return type
     */
    public static function getTopLoosers() {
        //calls Zanibal's soap method "getMarketLeaders"
        $result = webserviceModel::getWebServiceConnection()->getMarketLeaders(
                SecurityModel::$exchange, SecurityModel::$loosersRecordType, 0, 5
        );

        if (is_soap_fault($result)) {
            return $result->faultstring;
        } else {			
            $jsonData = json_encode($result, JSON_PARTIAL_OUTPUT_ON_ERROR);
            return json_decode($jsonData);
        }
    }

    /**
     * 
     * @return type
     */
    public static function getMarketHighLight() {
        $curl = curl_init(SecurityModel::$asiURL);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $curl_response = curl_exec($curl);

        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            return [];
            //return $info;
        }
        curl_close($curl);
        $data  = json_decode($curl_response);
        return $data;
    }

    /**
     * 
     * @param database result $row
     */
    private static function filterSecurities($row) {
        if (!in_array($row['id'], SecurityModel::$unWantedSecuritiesId, TRUE)) {
            array_push(SecurityModel::$securities, $row);
        }
    }

}
