<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace models;

use models\NIBBSModel;
use models\WebServiceModel as webserviceModel;
use models\EmailModel;

/**
 * Description of PaymentGatewayModel
 *
 * @author Olaleye
 */
class PaymentGatewayModel {

    private static $contraAccountIds = [
        'CSS' => 292,
        'CSP' => 52
    ];
    private $transactionId;
    private $chars;
    private $length; //Lenght of the Transaction ID
    public $productDescription;
    public $amount;
    public $clientId;
    public $cashAccountId;
    public $productType;
    public $reference;
    public $contraAcctId;
    public $companyName;
    private static $transState = "PENDING";
    private static $transType = "RECEIPT";
    private static $transMethod = "ECHANNEL";
    private static $currency = "NGN";
    private static $partnerName = "";
    private static $paymentGatewayId = 2;

    /**
     * Default constructor
     */
    public function __construct() {
        $this->transactionId = "";
        $this->chars = "0123456789";
        $this->length = 13; //Random number / Transaction ID lenght 
        $this->productDescription = "";
        $this->amount = 0;
        $this->clientId = "";
        $this->cashAccountId = "";
        $this->productType = "";
        $this->reference = "NIBBS DEPOSIT";
    }

    public function createPartnerCashTransaction() {
        //Create the cash transaction and use the reference number to submit the deposit to the gateway
        $cashTransaction = [
            //Transaction Status POSTED
            "transState" => self::$transState,
            // Transaction type which can be a Receipt or payment.A Receipt for Depositing and payment for withdrawal                                  
            "transType" => self::$transType,
            // The channel in which the transaction is initiated  from                                  
            "transMethod" => self::$transMethod,
            // The amount the client is requesting for.                                 
            "amount" => $this->amount / 100,
            "cashAccountName" => $this->cashAccountId,
            "currency" => self::$currency,
            //This is the client id
            "partnerId" => $this->clientId,
            // The name field from the client profile - this is optional if the ID is specified                                    
            "partnerName" => self::$partnerName,
            // The id of the payment gateway configured on Zanibal                   
            "paymentGatewayId" => self::$paymentGatewayId,
            // The reference field in the transaction                                     
            "reference" => $this->reference,
            // The bank account in which the second transaction originates from. This can be left blank for the system to use the default bank account configured in the company configuration for the user's cash account.
            // The $transaction_details->ProductId is the cashAccount number selected by the client                                        
            "contraAcctId" => self::$contraAccountIds[$this->companyName]
        ];

        $partnerCashTransactionId = webserviceModel::getWebServiceConnection()
                ->createPartnerCashTransaction($cashTransaction);
        webserviceModel::getWebServiceConnection()
                ->approvePartnerCashTransactionById($partnerCashTransactionId);
        webserviceModel::getWebServiceConnection()
                ->postPartnerCashTransactionById($partnerCashTransactionId);
    }

    public function getTransactionDetails() {
        $nibbs = new NIBBSModel();
        $transactionId = $this->generateTransactionId();
        $transactionIdIsUnique = $nibbs->isTransactionIdUnique($transactionId);

        if ($transactionIdIsUnique) {
            $nibbs->transactionId = $transactionId;
            $nibbs->clientId = $this->clientId;
            $nibbs->cashAccountId = $this->cashAccountId;
            $nibbs->productType = $this->productType;
            $nibbs->amount = $this->amount;
            $nibbs->productDescription = $this->productDescription;
            $nibbs->companyName = $this->companyName;
            // $this->createPartnerCashTransaction();
            return $nibbs->createTransaction();
        } else {
            $this->getTransactionDetails;
        }
    }

    public function postTransaction($transactionId, $cpayRef, $merchantId) {
        //$nibbs = new NIBBSModel();
        $result = NIBBSModel::postDataBackToNIBBS($transactionId, $cpayRef, $merchantId);
		//var_dump($result); die;
        if($result){
            //return $result;
            $this->createPartnerCashTransaction(); 
            return $result;           
            //EmailModel::sendMail($data);
        }else{
            return FALSE;
        }
//        $transactionId = $this->generateTransactionId();
//        $transactionIdIsUnique = $nibbs->isTransactionIdUnique($transactionId);
//
//        if ($transactionIdIsUnique) {
//            $nibbs->transactionId = $transactionId;
//            $nibbs->clientId = $this->clientId;
//            $nibbs->cashAccountId = $this->cashAccountId;
//            $nibbs->productType = $this->productType;
//            $nibbs->amount = $this->amount;
//            $nibbs->productDescription = $this->productDescription;
//            $this->createPartnerCashTransaction();
//            return $nibbs->createTransaction();
//        } else {
//            $this->postTransaction();
//        }
    }

    private function generateTransactionId() {
        $result = "";
        $charArray = str_split($this->chars);

        for ($i = 0; $i < $this->length; $i++) {
            $index = array_rand($charArray);
            $result .= $charArray[$index];
        }
        return $result;
    }

}
