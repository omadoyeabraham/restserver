<?php

/* --------------------------------------------------------------
  # WebServiceModel added By Olaleye Osunsanya, Date: August 22, 2017

  This class handles any Web Service connection. I did not use a Singleton
  because of obvious reasons.


  NOTE: Because my code looks beautiful does not mean they are good.
  Word for the wise: Silence is golden!
  WARNING: My comments might hurt your feelings!
  -------------------------------------------------------------- */

namespace models;

use SoapClient;

/**
 * Description of WebServiceModel
 *
 * @author Olaleye
 */
class WebServiceModel {
    /* --------------------------------------------------------------
      # Variables added By Olaleye Osunsanya, Date: August 22, 2017
     * 
     * This variables were declared static to avoid multiple connections
     * It makes it work like a Singleton
     * 
     * I believe it is a better implementation than the Singleton pattern
     * Please do not quote me
      -------------------------------------------------------------- */
    private static $connection;

    /**
     * Default Constructor
     */
    public function __construct() {
        
    }

    public static function getWebServiceConnection() {
        if (!isset(WebServiceModel::$connection)) {
            WebServiceModel::$connection = new SoapClient(
                'https://zanibal.cardinalstone.com/ebclient/services/MobileClientService?wsdl', 
                array(
                    "trace" => 1,
                    "exceptions" => 0,
                    "login" => 'root',
                    "password" => 'Csp#2017'
                    )
            );
        }

        return WebServiceModel::$connection;
    }

}
