<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace models;

use models\WebServiceModel as webserviceModel;
use models\TermInstrumentModel;
use models\CashAccountModel;
use models\StockbrokingModel;
use models\DatabaseModel as dbModel;
use models\EmailModel;

/**
 * Description of CustomerModel
 *
 * @author Olaleye
 */
class CustomerModel {
    /* --------------------------------------------------------------
      # Variables added By Olaleye Osunsanya, Date: August 22, 2017
      -------------------------------------------------------------- */

    private $id;
    private $portalUserName;
    private $customerData;
    private $data;
    private $termInstrumentModel;
    private $stockbrokingModel;
    private $cashAccountModel;

    /**
     * Default Constructor
     */
    public function __construct() {
        $this->id = "";
        $this->portalUserName = "";
        $this->customerData = [];
        $this->data = [];
        $this->stockbrokingModel = new StockbrokingModel();
        $this->cashAccountModel = new CashAccountModel();
        $this->termInstrumentModel = new TermInstrumentModel($this->cashAccountModel);
    }

    /**
     * 
     * @param type $name
     * @return type
     */
    public function findCustomerByName($name) {
        $this->customerData = webserviceModel::getWebServiceConnection()
                ->findCustomerByPortalUserName($name);
        unset($this->customerData->portalPassword);//Unset the user's password
        return $this->customerData;
    }
	
	/**
     * 
     * @param type $id
     * @return type
     */
    public function findCustomerDataById($id) {
        $this->customerData = webserviceModel::getWebServiceConnection()
                ->findCustomerById($id);
        return $this->customerData;
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function findCustomerById($id) {
        $data = [];
        $data['id'] = $id;
        $data['STB'] = $this->stockbrokingModel->findPortfoliosByCustomerId($id);
        $data['FI'] = $this->termInstrumentModel->findCustomerTermInstruments($id);
        $data['CA'] = $this->cashAccountModel->findCustomerFiAccts($id);

        return $data;
    }

    /**
     * 
     * @param string $name
     * @param string $password
     * @return array
     */
    public function login($name, $password) {
        $this->customerData = webserviceModel::getWebServiceConnection()
                ->findCustomerByPortalUserName($name);
				
				//var_dump($this->customerData); die;

        if (($this->customerData->active) &&
                ($this->customerData->portalUserName === $name) &&
                ($this->customerData->portalPassword === $password) &&
                (!empty($this->customerData->portalPasswordToken))) {

            $this->id = $this->customerData->id;            
            unset($this->customerData->portalPassword);//Unset the user's password
            $this->data['customer'] = $this->customerData;
            $this->data['STB'] = $this->stockbrokingModel->findPortfoliosByCustomerId($this->id);
            $this->data['FI'] = $this->termInstrumentModel->findCustomerTermInstruments($this->id);
            $this->data['CA'] = $this->cashAccountModel->findCustomerFiAccts($this->id);

            apcu_store((string)$this->id, $this->customerData);
            apcu_store($this->customerData->portalPasswordToken, $this->id);
        }
        return $this->data;
    }
    
    /**
     * 
     * @param int $id
     * @param string $token
     * @return boolean
     */
    public function logout($id, $token){
        apcu_delete($id);
        $result = apcu_delete($token);  
        return $result;
    }
	
	/**
     * 
     * @param int $id
     * @param string $oldPassword
     * @param string $newPassword
     * @return array
     */
    public function resetPassword($id, $oldPassword, $newPassword) {
        if (isset($id) && !empty($id) && isset($oldPassword) && !empty($oldPassword) && isset($newPassword) && !empty($newPassword)) {
            $result = webserviceModel::getWebServiceConnection()
                    ->updateCustomerPassword($id, $oldPassword, $newPassword);
            if (is_soap_fault($result)) {
                return [];
            } else {
                return ['status' => 'Password Changed Successfuly'];
            }
        } else {
            return [];
        }
    }

    /**
    * Send a link to the customer which enables the reset their password
    *
    */
    public function sendResetPasswordLink($username, $email, $userId, $userLabel) {
        $randomCode = md5(urlencode(rand().$username."&".$email."&".time()));

        $query = "INSERT INTO client_portal.password_resets (`token`, `username`, `user_id`) VALUES ('$randomCode', '$username', $userId);";
        $result = dbModel::getClientPortalConnection()->query($query);

        // An error occured while saving the reset code into the DB
        if(!$result) {
            return ["error" => 'Unable to save reset code'];
        }

        // $link = "http://localhost:8080/#/login?resetLink=".$randomCode;
        $link = "https://portal.cardinalstone.com/broker/desktop/#/login?resetLink=".$randomCode;

        EmailModel::sendMail([
            'userEmail' => $email,
            'userName' => $username,
            'link' => $link,
            'userLabel' => $userLabel,
        ], "resetPassword");

        return ['code' => $randomCode, 'username' => $username, 'result' => $result, 'query' => $query];
    }

    /**
    * Check the DB and retrieve the data for a previous password reset request
    *
    * @param String $resetCode  The resetcode sent to the client
    */
    public function retrievePasswordResetData($resetCode = "") {
        $query = "SELECT * FROM client_portal.password_resets WHERE token = '$resetCode';";

        $result = dbModel::getClientPortalConnection()->query($query);
        //return $query;
        if ($result->num_rows != 0) {
        
            return $result->fetch_assoc();
        } else {
            return [
                'isValid' => false,
                'message' => 'Reset link record not found in database',
                'resetCode' => $resetCode
            ];
        }
    }

    /**
     * 
     * @param type $token
     * @return type
     */
    public function authenticate($token) {
        return (apcu_exists($token)) ? true : false;
    }

}
