<?php

/* --------------------------------------------------------------
  # TBillModel added By Olaleye Osunsanya, Date: August 21, 2017

  This class handles everything on the Dashboard of the Trade Portal


  NOTE: Because my code looks beautiful does not mean they are good.
  Word for the wise: Silence is golden!
  WARNING: My comments might hurt your feelings!
  -------------------------------------------------------------- */

namespace models;

use models\DatabaseModel as dbModel;

/**
 * Description of TBillModel
 *
 * @author Olaleye
 */
class TBillModel {
    /* --------------------------------------------------------------
      # Variables added By Olaleye Osunsanya, Date: August 23, 2017
      -------------------------------------------------------------- */

    private $treasuryBills;
    private $status;
    private $userId;
	private static $label = "'TREASURY BILLS INVESTMENTS' as instrumentTypeLabel";

    /**
     * Default Constructor
     */
    public function __construct() {
        $this->treasuryBills = [];
        $this->status = "RUNNING";
        $this->userId = "";
    }

    /**
     * 
     * @param int $userId
     * @param string $status
     * @return array
     */
    public function findTBills($userId, $status = NULL) {
        $query = "SELECT t.id, t.label, t.discountRate AS currentRate, 
                    t.discountedValue_amount AS faceValue, t.maturityDate AS expectedMaturity,
                    t.faceValue_amount AS faceValueAmount, t.startdate AS startDate,
                    t.status, t.tenure, t.interestRate AS currentRate, 
                    t.terminateDate, ". TBillModel::$label ." 
                    FROM zebroker_prod.tbilltransaction t
                    WHERE t.customer_id = $userId;";

        $result = dbModel::getMySQLiDBConnection()->query($query);

        if ($result->num_rows != 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        } else {
            return [];
        }
    }

}
