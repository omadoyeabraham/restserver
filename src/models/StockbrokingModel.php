<?php

/* --------------------------------------------------------------
  # StockbrokingModel added By Olaleye Osunsanya, Date: August 21, 2017

  This class handles everything Stockbroking


  NOTE: Because my code looks beautiful does not mean they are good.
  Word for the wise: Silence is golden!
  WARNING: My comments might hurt your feelings!
  -------------------------------------------------------------- */

/**
 * Description of StockbrokingModel
 *
 * @author Olaleye Osunsanya
 */

namespace models;

use models\WebServiceModel as webserviceModel;

class StockbrokingModel {
    /* --------------------------------------------------------------
      # Variables added By Olaleye Osunsanya, Date: August 21, 2017
      -------------------------------------------------------------- */

    private $stockbrokingPortfolios = []; //holds all the portfolios
    private $managedPortfolios = []; //SMA Portfolios
    private $exchangePortfolios = []; //User Portfolios

    /**
     * Default constructor
     */
    public function __construct() {
        $this->stockbrokingPortfolios = [];
        $this->managedPortfolios = [];
        $this->exchangePortfolios = [];
    }

    /**
     * This method returns both the SMA and Exchange Portfolios
     *
     * @author Olaleye Osunsanya
     *
     * @param int $id The client Id
     * */
    function findPortfoliosByCustomerId($id) {
        //calls Zanibal's soap method "findCustomerById"
        $portfolios = webserviceModel::getWebServiceConnection()->findCustomerPortfolios($id);

        /**
         *
         * Checks that the portfolios returned is an array 
         * if not it returns an array
         * 
         * */
        if(isset($portfolios->item) && is_array($portfolios->item)){
            $portfolios_array = $portfolios->item;
        }
        
        if(isset($portfolios->item) && !is_array($portfolios->item)){
            $portfolios_array = [$portfolios->item];
        }
        
        if (isset($portfolios_array) && !empty($portfolios_array)) {
            $this->stockbrokingPortfolios [SMA] = $this->getManagedPortfolios($portfolios_array); //SMA accounts
            $this->stockbrokingPortfolios [USER_TRADE] = $this->getExchangePortfolios($portfolios_array); // Exchange accounts
        }

        return $this->stockbrokingPortfolios;
    }

    /**
     * This method returns both SMA Portfolios
     *
     * @author Olaleye Osunsanya
     *
     * @param array $portfolios_array The client portfolios
     * */
    function getManagedPortfolios($portfolios_array = []) {

        if (!empty($portfolios_array)) {
            foreach ($portfolios_array as $key => $value) {
                if ($portfolios_array[$key]->portfolioClass === SMA) {
                    array_push($this->managedPortfolios, $portfolios_array[$key]);
                }
            }
            return $this->managedPortfolios;
        }
    }

    /**
     * This method returns both Exchange Portfolios
     *
     * @author Olaleye Osunsanya
     *
     * @param array $portfolios_array The client portfolios
     * */
    function getExchangePortfolios($portfolios_array = []) {

        if (!empty($portfolios_array)) {
            foreach ($portfolios_array as $key => $value) {
                if ($portfolios_array[$key]->portfolioClass === USER_TRADE) {
                    array_push($this->exchangePortfolios, $portfolios_array[$key]);
                }
            }

            return $this->exchangePortfolios;
        }
    }

}
