<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace models;

use models\DatabaseModel as dbModel;
use models\SecurityModel as securityModel;
use models\EmailModel;
use models\CustomerModel;

/**
 * Description of WatchListModel
 *
 * @author Olaleye
 */
class WatchListModel {

    //put your code here
    public $id; //Wacthlist Id
    public $name; //Watchlist name    
    public $customerId;
    public $securityName;
    public $price;
    public $condition;
    public $conditionName;
    public static $conditions = [
        '<=' => 'Less Than or Equals',
        '>=' => 'Greater Than or Equals'
    ];
    public $status = 1;
    private $dateCreated;
    private $dateUpdated;
    private $watchLists = [];

    public function __construct() {
        $this->name = "";
        $this->customerId = "";
        $this->securityName = "";
        $this->price = "";
        $this->condition = "";
        $this->conditionName = "";
    }

    /**
     * 
     * @return array
     */
    public function getWatchList() {
        $query = "SELECT * FROM client_portal.watchlist
                    WHERE user_id = $this->customerId;";
        $result = dbModel::getClientPortalConnection()->query($query);

        if ($result->num_rows != 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($this->watchLists, $row);
            }
            return $this->watchLists;
        } else {
            return [];
        }
    }

    /**
     * 
     * @return boolean
     */
    public function createWatchList() {
        $this->dateCreated = "'" . date('Y-m-d') . "'";

        $query = "INSERT INTO client_portal.watchlist (
                    `user_id`, `name`, `notify_price`, `condition`, `condition_name`, 
                    `status`, `date_created`, `watchlist_name`) 
                    VALUES ($this->customerId, $this->securityName, $this->price,
                            $this->condition, $this->conditionName, $this->status,
                            $this->dateCreated, $this->name);";

        dbModel::getClientPortalConnection()->query($query);
        $insertId = mysqli_insert_id(dbModel::getClientPortalConnection());

        return ($insertId > 0) ? TRUE : FALSE;
    }

    /**
     * 
     * @return boolean
     */
    public function editWatchList() {
        $this->dateUpdated = "'" . date('Y-m-d') . "'";

        $query = "UPDATE client_portal.watchlist
                    SET `name` = $this->securityName,
                        `notify_price` = $this->price,
                        `condition` = $this->condition,
                        `condition_name` = $this->conditionName,
                        `status` = $this->status,
                        `date_updated` = $this->dateUpdated,
                        `watchlist_name` = $this->name
                    WHERE `id` = $this->id AND `user_id` = $this->customerId;";

        dbModel::getClientPortalConnection()->query($query);
        $affectedRows = mysqli_affected_rows(dbModel::getClientPortalConnection());

        return ($affectedRows > 0) ? TRUE : FALSE;
    }

    /**
     * 
     * @return boolean
     */
    public function deleteWatchList() {
        $query = "DELETE FROM client_portal.watchlist
                    WHERE `id` = $this->id AND `user_id` = $this->customerId;";

        dbModel::getClientPortalConnection()->query($query);
        $affectedRows = mysqli_affected_rows(dbModel::getClientPortalConnection());

        return ($affectedRows > 0) ? TRUE : FALSE;
    }

    /**
     * 
     * @return boolean
     */
    public function toggleWatchList() {
        $query = "UPDATE client_portal.watchlist
                    SET `status` = $this->status
                    WHERE `id` = $this->id AND `user_id` = $this->customerId;";

        dbModel::getClientPortalConnection()->query($query);
        $affectedRows = mysqli_affected_rows(dbModel::getClientPortalConnection());

        return ($affectedRows > 0) ? TRUE : FALSE;
    }

    private static function getAll() {
        $allWatchList = [];
        $query = "SELECT * FROM client_portal.watchlist WHERE status = 1;";
        $result = dbModel::getClientPortalConnection()->query($query);

        if ($result->num_rows != 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($allWatchList, $row);
            }
            return $allWatchList;
        } else {
            return [];
        }
    }

    /**
     * 
     */
    public static function sendWatchListNotification() {
        $securityNames = securityModel::getAllSecurityNames();
        $allWatchList = self::getAll();

        foreach ($allWatchList as $key => $value) {
            switch ($value['condition_name']) {
                case 'Less Than':
                    self::doLessThan($securityNames, $value);
                    break;
                case 'Less Than or Equals':
                    self::doLessThanEquals($securityNames, $value);
                    break;
                case 'Greater Than or Equals':
                    self::doGreaterThanEquals($securityNames, $value);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 
     * @param type $securityName
     * @param type $watchListValue
     */
    private static function doLessThan($securityName, $watchListValue) {
        if ($securityName[$watchListValue['name']]['lastTradePrice'] < $watchListValue['notify_price']) {
            $data = [
                'security' => $watchListValue['name'],
                'currentPrice' => $securityName[$watchListValue['name']]['lastTradePrice'],
                'condition' => $watchListValue['condition'],
                'watchListPrice' => $watchListValue['notify_price']
            ];
                
            $result = self::updateWatchListStatus($watchListValue['id'], $watchListValue['user_id']);
            if ($result) {
                self::sendEmail($watchListValue['user_id'], $data);
            }
        }
    }

    /**
     * 
     * @param type $securityName
     * @param type $watchListValue
     */
    private static function doLessThanEquals($securityName, $watchListValue) {
        if ($securityName[$watchListValue['name']]['lastTradePrice'] <= $watchListValue['notify_price']) {
            $data = [
                'security' => $watchListValue['name'],
                'currentPrice' => $securityName[$watchListValue['name']]['lastTradePrice'],
                'condition' => $watchListValue['condition'],
                'watchListPrice' => $watchListValue['notify_price']
            ];
                
            $result = self::updateWatchListStatus($watchListValue['id'], $watchListValue['user_id']);
            if ($result) {
                self::sendEmail($watchListValue['user_id'], $data);
            }
        }
    }

    /**
     * 
     * @param type $securityName
     * @param type $watchListValue
     */
    private static function doGreaterThanEquals($securityName, $watchListValue) {
        if ($securityName[$watchListValue['name']]['lastTradePrice'] >= $watchListValue['notify_price']) {
            $data = [
                'security' => $watchListValue['name'],
                'currentPrice' => $securityName[$watchListValue['name']]['lastTradePrice'],
                'condition' => $watchListValue['condition'],
                'watchListPrice' => $watchListValue['notify_price']
            ];
                
            $result = self::updateWatchListStatus($watchListValue['id'], $watchListValue['user_id']);
            if ($result) {
                self::sendEmail($watchListValue['user_id'], $data);
            }
        }
    }

    /**
     * 
     * @param type $id
     * @param type $customerId
     * @return type
     */
    private static function updateWatchListStatus($id, $customerId) {
        $dateUpdated = "'" . date('Y-m-d') . "'";
        $query = "UPDATE client_portal.watchlist
                    SET `status` = 0,
                        `date_updated` = $dateUpdated
                    WHERE `id` = $id AND `user_id` = $customerId;";

        dbModel::getClientPortalConnection()->query($query);
        $affectedRows = mysqli_affected_rows(dbModel::getClientPortalConnection());
//        var_dump($query);
//        var_dump($affectedRows);die;

        return ($affectedRows > 0) ? TRUE : FALSE;
    }

    private static function sendEmail($customerId, $data) {
        $customerModel = new CustomerModel();
        $customer = $customerModel->findCustomerDataById($customerId);
        EmailModel::sendMail([
            'userName' => $customer->label,
            'userEmail' => $customer->emailAddress1,
            'security' => $data['security'],
            'currentPrice' => $data['currentPrice'],
            'condition' => $data['condition'],
            'watchListPrice' => $data['watchListPrice']
                ], "watchList");
    }

}
