<?php

/* --------------------------------------------------------------
  # ForeignExchangeModel added By Olaleye Osunsanya, Date: August 21, 2017

  This class handles everything on the Dashboard of the Trade Portal


  NOTE: Because my code looks beautiful does not mean they are good.
  Word for the wise: Silence is golden!
  WARNING: My comments might hurt your feelings!
  -------------------------------------------------------------- */

/**
 * Description of DashboardModel
 *
 * @author Olaleye Osunsanya
 */

namespace models;

/**
 * Description of ForeignExchangeModel
 *
 * @author Olaleye
 */
class ForeignExchangeModel {
   
    private static $url = "https://openexchangerates.org/api/latest.json?app_id=f42021b55ab2469ab40bdde718e33a4e&base=USD";
    private static $NGNRate = "";
    private static $GBPRate = "";
    private static $EURRate = "";
    
    public function __construct() {
        
    }
    
    public static function getForeignExchangeData(){
        
        if(apcu_exists('foreignExchange')){
            return apcu_fetch('foreignExchange');
        }        
        
        $curl = curl_init(self::$url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $curl_response = curl_exec($curl);

        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            return [];
            //return $info;
        }
        curl_close($curl);
        //var_dump($curl_response);    
        $data = json_decode($curl_response);
        $result = [
            'timestamp' => $data->timestamp,
            'base' => 'NGN',
            'USDRate' => $data->rates->NGN,
            'GBPRate' => ($data->rates->NGN / $data->rates->GBP),
            'EURRate' => ($data->rates->NGN / $data->rates->EUR),
            'CADRate' => ($data->rates->NGN / $data->rates->CAD),
            'CNYRate' => ($data->rates->NGN / $data->rates->CNY),
            'AUDRate' => ($data->rates->NGN / $data->rates->AUD)
        ];
        //var_dump($data);
        //var_dump($data->rates->NGN);
        //die;
        apcu_store('foreignExchange', $result, 86400);
        return $result;
    }
}
