<?php

/* --------------------------------------------------------------
  # DashboardModel added By Olaleye Osunsanya, Date: August 21, 2017

  This class handles everything on the Dashboard of the Trade Portal


  NOTE: Because my code looks beautiful does not mean they are good.
  Word for the wise: Silence is golden!
  WARNING: My comments might hurt your feelings!
  -------------------------------------------------------------- */

/**
 * Description of DashboardModel
 *
 * @author Olaleye Osunsanya
 */

namespace models;

use models\SecurityModel;
use models\NewsModel;
use models\ForeignExchangeModel;

/**
 * Description of DashboardModel
 *
 * @author Olaleye
 */
class DashboardModel {
    
    /* --------------------------------------------------------------
      # Variables added By Olaleye Osunsanya, Date: August 21, 2017
      -------------------------------------------------------------- */
//    private $stockbrokingModel;
//    private $termInstrumentModel;
//    private $cashAccountModel;
     private static $dashboard = [];
//    p    private $dashboard = [];rivate $mdsASI = "https://mds.zanibal.com/mds/rest/api/v1/research/get-security-overview/symbol?x=NSE&s=ASI";
//    private static $connection;
    
    /**
     * Default constructor
     */
    public function __construct() {

    }
    
    /**
     * 
     */
    public static function getDashboardData(){
        DashboardModel::$dashboard['MARKETHIGHLIGHTS'] = SecurityModel::getMarketHighLight();
        DashboardModel::$dashboard['NSEASI'] = SecurityModel::getAsiFiveDaysData();
        DashboardModel::$dashboard['TOPGAINERS'] = SecurityModel::getTopGainers();
        DashboardModel::$dashboard['TOPLOSERS'] = SecurityModel::getTopLoosers();
        DashboardModel::$dashboard['BUSINESSNEWS'] = NewsModel::getGoogleNews();
		DashboardModel::$dashboard['FOREIGNEXCHANGE'] = ForeignExchangeModel::getForeignExchangeData();
        
        return DashboardModel::$dashboard;
    }
    
    
    
    
}
