<?php

/* --------------------------------------------------------------
  # OrderModel added By Olaleye Osunsanya, Date: August 22, 2017

  This class handles customer's order


  NOTE: Because my code looks beautiful does not mean they are good.
  Word for the wise: Silence is golden!
  WARNING: My comments might hurt your feelings!
  -------------------------------------------------------------- */

namespace models;

use models\WebServiceModel as webserviceModel;

/**
 * Description of OrderModel
 *
 * @author Olaleye
 */
class OrderModel {
    /* --------------------------------------------------------------
      # Variables added By Olaleye Osunsanya, Date: August 25, 2017
      -------------------------------------------------------------- */

    public $customerId;
    public $startRecord;
    public $endRecord;
    public $fixStatus;
    public $orderStatus;
    public $startDate;
    public $endDate;
    public $cacheTrigger;

    /**
     * Default Constructor
     */
    public function __construct() {
        $this->customerId = "";
        $this->startRecord = 0;
        $this->endRecord = 500;
        $this->fixStatus = "";
        $this->orderStatus = "";
        $this->startDate = "";
        $this->endDate = "";
        $this->cacheTrigger = "1"; //Turn the cache trigger on
    }

    /**
     * 
     * @return type
     */
    public function findCustomerOrders() {
        //calls Zanibal's soap method "findCustomerOrders"
        $cacheKey = 'orders' . $this->customerId;

        if ($this->cacheTrigger === "1" && apcu_exists($cacheKey)) {
            return apcu_fetch($cacheKey);
        } 

        if (($this->cacheTrigger === "1" && !apcu_exists($cacheKey)) || $this->cacheTrigger === "0") {
            $result = webserviceModel::getWebServiceConnection()->findCustomerOrders(
                    $this->customerId, $this->startRecord, $this->endRecord
            );

            if (is_soap_fault($result)) {
                return $result->faultstring;
            } else {
                apcu_store($cacheKey, $result);
                return $result;
            }
        }
    }
	
	public function searchCustomerOrders() {
        $result = webserviceModel::getWebServiceConnection()->findCustomerOrders(
                $this->customerId, $this->startRecord, 500, NULL, NULL, $this->startDate, $this->endDate
        );

        if (is_soap_fault($result)) {
            return $result->faultstring;
        } else {
            return $result;
        }
    }

}
