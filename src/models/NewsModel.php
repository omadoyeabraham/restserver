<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace models;

/**
 * Description of GoogleNewsModel
 *
 * @author Olaleye
 */
class NewsModel {

    private static $googleNewsURL = "https://news.google.com/news?pz=1&cf=all&ned=en_ng&hl=en&topic=b&output=rss";
    private static $newsLimit = 7;
    private static $googleNews = [];

    public function __construct() {
        
    }

    /**
     * 
     * @return type
     */
    public static function getGoogleNews() {
		NewsModel::$googleNews = simplexml_load_file(NewsModel::$googleNewsURL);
        $news = NewsModel::$googleNews->channel;
        unset($news->generator);
        unset($news->title);
        unset($news->link);
        unset($news->language);
        unset($news->webMaster);
        unset($news->copyright);
        unset($news->lastBuildDate);
        unset($news->image);
            
        $data = json_decode(json_encode((array)$news), TRUE);             
        return $data;
    }

}
