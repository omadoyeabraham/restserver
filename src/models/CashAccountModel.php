<?php

/* --------------------------------------------------------------
  # CashAccountModel added By Olaleye Osunsanya, Date: August 22, 2017

  This class handles a cleint's Cash Account


  NOTE: Because my code looks beautiful does not mean they are good.
  Word for the wise: Silence is golden!
  WARNING: My comments might hurt your feelings!
  -------------------------------------------------------------- */

namespace models;

use models\WebServiceModel as webserviceModel;

/**
 * Description of CashAccountModel
 *
 * @author Olaleye
 */
class CashAccountModel {
    /* --------------------------------------------------------------
      # Variables added By Olaleye Osunsanya, Date: August 22, 2017
      -------------------------------------------------------------- */

    private $fiAccounts = [];
    private $nairaRateCashAccountId;
    private $NGNCashAccounts = []; //NGN Cash Accounts
    private $USDCashAccounts = []; //USD Cash Accounts
    public static $customerId;
    public static $startRecord;
    public static $endRecord;
    public static $startDate;
    public static $endDate;
	public static $accountNumber;

    /**
     * Default Constructor
     */
    public function __construct() {
        $this->fiAccounts = [];
        $this->nairaRateCashAccountId = "";
        $this->NGNCashAccounts = [];
        $this->USDCashAccounts = []; //USD Cash Accounts
    }

    /**
     * 
     * @param int $id
     * @return array
     */
    function findCustomerFiAccts($id) {
        //calls Zanibal's soap method "findCustomerFiAccts"
        $cashAccounts = webserviceModel::getWebServiceConnection()->findCustomerFiAccts($id);

        /**
         *
         * Checks that the data returned is an array 
         * if not it returns an array
         * 
         * */
        if (isset($cashAccounts->item) && is_array($cashAccounts->item)) {
            $cashAccounts_array = $cashAccounts->item;
        }

        if (isset($cashAccounts->item) && !is_array($cashAccounts->item)) {
            $cashAccounts_array = [$cashAccounts->item];
        }

        if (isset($cashAccounts_array) && !empty($cashAccounts_array)) {
            $this->fiAccounts['NGN'] = $this->getNGNCashAccounts($cashAccounts_array);
            $this->fiAccounts['USD'] = $this->getUSDCashAccounts($cashAccounts_array);
        }

        return $this->fiAccounts;
    }

    /**
     * 
     * @param int $id
     * @return array
     */
    function findCustomerNariaRateCashAccountId($id) {

        //calls Zanibal's soap method "findCustomerById"
        $cashAccounts = webserviceModel::getWebServiceConnection()->findCustomerFiAccts($id);

        /**
         *
         * Checks that the data returned is an array 
         * if not it returns an array
         * 
         * */
        if (isset($cashAccounts->item) && is_array($cashAccounts->item)) {
            $cashAccounts_array = $cashAccounts->item;
        }

        if (isset($cashAccounts->item) && !is_array($cashAccounts->item)) {
            $cashAccounts_array = [$cashAccounts->item];
        }

        if (isset($cashAccounts_array) && !empty($cashAccounts_array)) {

            foreach ($cashAccounts_array as $key => $value) {
                if ($cashAccounts_array[$key]->currency === DOLLAR &&
                        $cashAccounts_array[$key]->companyLabel === CSP &&
                        ( strpos($cashAccounts_array[$key]->label, "EIN") !== false )
                ) {
                    $this->nairaRateCashAccountId = $cashAccounts_array[$key]->id;
                }
            }
        }

        return $this->nairaRateCashAccountId;
    }

    /**
     * 
     * @param type $cashAccounts_array
     * @return type
     */
    function getNGNCashAccounts($cashAccounts_array = []) {
        if (!empty($cashAccounts_array)) {
            foreach ($cashAccounts_array as $key => $value) {
                if ($cashAccounts_array[$key]->currency === NAIRA) {
                    array_push($this->NGNCashAccounts, $cashAccounts_array[$key]);
                }
            }

            return $this->NGNCashAccounts;
        }
    }

    /**
     * 
     * @param type $cashAccounts_array
     * @return type
     */
    function getUSDCashAccounts($cashAccounts_array = []) {
        if (!empty($cashAccounts_array)) {
            foreach ($cashAccounts_array as $key => $value) {
                if ($cashAccounts_array[$key]->currency === DOLLAR) {
                    array_push($this->USDCashAccounts, $cashAccounts_array[$key]);
                }
            }

            return $this->USDCashAccounts;
        }
    }

    /**
     * 
     * @return type
     */
    public static function findCustomerLedgerEntries() {
        //calls Zanibal's soap method "findCustomerFiAccts"
        $ledgerEntries = webserviceModel::getWebServiceConnection()
                ->findCustomerLedgerEntries(
                [
                    'arg0' => CashAccountModel::$customerId,
                    'arg1' => CashAccountModel::$startRecord,
                    'arg2' => CashAccountModel::$endRecord,
                    'arg3' => CashAccountModel::$startDate,
                    'arg4' => CashAccountModel::$endDate
                ]
        );

        if (is_soap_fault($ledgerEntries)) {
            return $ledgerEntries->faultstring;
        } else {
            return $ledgerEntries;
        }
    }
	
	/**
     * 
     * @return type
     */
    public static function findFiAcctLedgerEntriesByAccountNumber() {
        //calls Zanibal's soap method "findCustomerFiAccts"
        $result = webserviceModel::getWebServiceConnection()
                ->findFiAcctLedgerEntriesByAccountNumber(
                        CashAccountModel::$accountNumber, 
                        CashAccountModel::$startRecord, 
                        CashAccountModel::$endRecord, 
                        CashAccountModel::$startDate, 
                        CashAccountModel::$endDate
        );

        if (is_soap_fault($result)) {
            return $result->faultstring;
        } else {
            return $result;
        }
    }

}
